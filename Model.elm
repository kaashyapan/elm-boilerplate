module Model exposing(..)

type alias Model =
    String
    
init : ( Model, Cmd Msg )
init =
    ( "Hello World", Cmd.none )    
    
type Msg
    = NoOp    