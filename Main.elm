import View exposing(..)
import Model exposing(..)
import Update exposing(..)
import Html exposing(program)

main : Program Never Model Msg
main =
    program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
        

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none